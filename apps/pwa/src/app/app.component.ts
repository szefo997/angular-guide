import {HttpClient} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';

import {Post} from './post.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public posts: Post[] = [];

  constructor(
    private readonly _httpClient: HttpClient
  ) {
  }

  public ngOnInit(): void {
    this._httpClient
      .get<Post[]>('https://jsonplaceholder.typicode.com/posts')
      .subscribe((fetchedPosts: Post[]) => {
        this.posts = fetchedPosts;
      });
  }
}
